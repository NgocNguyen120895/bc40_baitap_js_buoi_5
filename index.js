//Bai tap 1

var n = 0;
var sum = 0;

while (sum < 10000) {
    n++;
    sum = sum + n;

}

document.getElementById('output-1').innerHTML = ` <div style="border-radius: 3px;background-color: green; color: white; width: 25%;">So nguyen duong nho nhat la ${n} </div>`;
// console.log(sum + " | " + n);


// Bai tap 2
function tinhTong() {
    var x = document.getElementById('input-x').value * 1;
    var n = document.getElementById('input-n').value * 1;
    var sum = 0;

    for (var i = 1; i <= n; i++) {

        sum += Math.pow(x, i)
    }

    document.getElementById('output-2').innerHTML = `Tong la: ${sum}`

}


// Bai tap 3
function tinhGiaiThua() {
    var m = document.getElementById('input-bt3-n').value * 1;
    var i = 1;
    var f = 1;
    while (i <= m) {
        f = f * i;
        i++;
    }
    var result = document.getElementById('output-3');
    // console.log(f);
    result.style.background = "pink";
    result.style.width = "25%";
    result.style.borderRadius = "5px";
    result.innerHTML = f;
}

//Bai tap 4s

var content = "";

for (var i = 0; i <= 10; i++) {
    if (i % 2 == 0) {
        content += "<div class='bg-danger text-white'>Div chan</div>";
    } else {
        content += "<div class='bg-primary text-white' >Div le</div>";
    }

    document.getElementById('output-4').innerHTML = content;


}